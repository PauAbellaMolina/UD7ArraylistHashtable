package ejercicio2;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Ejercicio2App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String cantidadProductosInput = JOptionPane.showInputDialog(null, "Cuantos productos vas a comprar?");
		int cantidadProductosInt = Integer.parseInt(cantidadProductosInput);
		
		double precioTotalProductos = 0;
		
		//Creamos el array para almacenar los datos del producto
		String[] arrayProducto = new String[4];
		
		//Creamos el ArrayList para almacenar cada array con los datos del producto
		ArrayList<String[]> lista = new ArrayList<>();
		
		//Bucle que pregunta al usuario el nombre, IVA y precio
		for (int contProductos = 0; contProductos < cantidadProductosInt; contProductos++) {
			String nombreProductoInput = JOptionPane.showInputDialog(null, "Introduce el nombre del producto:");
			arrayProducto[0] = nombreProductoInput;
			
			String ivaProductoInput = "";
			do {
				ivaProductoInput = JOptionPane.showInputDialog(null, "Introduce el IVA aplicado (21% o 4%):");
			} while(!ivaProductoInput.contentEquals("21") && !ivaProductoInput.contentEquals("4"));
			arrayProducto[1] = ivaProductoInput;
			
			String precioProductoInput = JOptionPane.showInputDialog(null, "Introduce el precio del producto sin IVA:");
			arrayProducto[2] = precioProductoInput;
			
			//Calculamos el precio con IVA
			double precioTotalIva = Double.parseDouble(precioProductoInput)+(Double.parseDouble(precioProductoInput)*Double.parseDouble(ivaProductoInput)/100);
			arrayProducto[3] = Double.toString(precioTotalIva);
			precioTotalProductos += precioTotalIva;
			
			//Metodo que guarda dentro del ArrayList el array con los datos que nos acaba de introducir el usuario
			guardarDatos(arrayProducto, lista);
			
			//"Limpiamos" el array para el siguiente paso del bucle
			arrayProducto = new String[4];
		}
		
		String cantidadPagadaInput = "";
		double cantidadPagadaInt = 0;
		//Controlamos que el dinero que nos da el cliente es mas que la cantidad a pagar
		do {
			cantidadPagadaInput = JOptionPane.showInputDialog(null, "Cantidad a pagar: " + precioTotalProductos + ". Dame el dinero:");
			cantidadPagadaInt = Double.parseDouble(cantidadPagadaInput);
		} while(cantidadPagadaInt < precioTotalProductos);
		
		//Calculamos el cambio a devolver
		double cambioDevolver = cantidadPagadaInt - precioTotalProductos;
		
		//Llamamos al metodo que muestra los datos por consola
		mostrarDatos(lista, cantidadProductosInt, cantidadPagadaInt, cambioDevolver);		
	}
	
	//Metodo que anade el array recibido dentro del ArrayList
	public static void guardarDatos(String[] arrayProducto, ArrayList<String[]> lista) {
		lista.add(arrayProducto);
	}
	
	//Metodo que muestra los datos recibidos por consola
	public static void mostrarDatos(ArrayList<String[]> lista, int cantidadProductos, double cantidadPagada, double cambioDevolver) {
		System.out.println("-PRODUCTOS-");
		
		//Bucle que recorre cada posicion del ArrayList (es decir cada array de producto) y nos printea por consola los datos de cada producto
		for (int i = 0; i < cantidadProductos; i++) {
			System.out.println("Nombre: " + lista.get(i)[0]);
			System.out.println("Tipo IVA: " + lista.get(i)[1]);
			System.out.println("Precio Bruto: " + lista.get(i)[2]);
			System.out.println("Precio con IVA: " + lista.get(i)[3]);
			System.out.println("");
			System.out.println("-----------------");
			System.out.println("");
		}
		
		System.out.println("Numero de articulos comprados: " + cantidadProductos);
		System.out.println("Cantidad Pagada: " + cantidadPagada);
		System.out.println("Cambio a devolver: " + cambioDevolver);
		System.out.println("");
	}	
}
