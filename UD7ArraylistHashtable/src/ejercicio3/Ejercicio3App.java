package ejercicio3;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.swing.JOptionPane;

public class Ejercicio3App {
	public static void main(String[] args) {
		//Creamos el hashtable y lo rellenamos con 10 productos de antemano
		Hashtable<String, Double> contenedorProductos = new Hashtable<String, Double>(){
		    {
		        put("iPhone X",1099.0);
		        put("Play Station 5",499.0);
		        put("EarPods",14.99);
		        put("Samsung Note 10",749.0);
		        put("Lenovo Thinkpad",999.99);
		        put("Logitech G403",34.99);
		        put("Ducky One TKL",139.0);
		        put("Audio Technica ATH-M40x",89.0);
		        put("Nvidia RTX 2080",799.99);
		        put("Windows 10 Key",109.0);
		    }
		};
		
		//Pedimos que el usuario nos introduzca una opcion del menu
		String opcionMenuInput;
		
		do {
			opcionMenuInput = JOptionPane.showInputDialog(null, "1) A�adir / 2) Mostrar Producto / 3) Mostrar Todos / 4) Salir");
			
			switch (opcionMenuInput) {
				case "1":
					//Llamamos al metodo de anadir producto
					anadirProducto(contenedorProductos);
					break;
				case "2":
					//Llamamos al metodo de mostrar un producto concreto
					mostrarProducto(contenedorProductos);
					break;
				case "3":
					//Llamamos al metodo de mostrar todos los productos
					mostrarTodos(contenedorProductos);
					break;
			}
		} while(!opcionMenuInput.contentEquals("4"));
	}
	
	//Metodo que pregunta al usuario el nombre y precio de un producto nuevo y lo anade al hashtable
	public static void anadirProducto(Hashtable<String, Double> contenedorProducto) {
		String nombreProductoInput = JOptionPane.showInputDialog(null, "Nombre del producto:");
		String precioProductoInput = JOptionPane.showInputDialog(null, "Precio del producto:");
		double precioProductoInt = Double.parseDouble(precioProductoInput);
		
		contenedorProducto.put(nombreProductoInput, precioProductoInt);
	}
	
	//Metodo que pregunta al usuario el nombre del producto a mostrar y muestra su nombre y precio por consola
	public static void mostrarProducto(Hashtable<String, Double> contenedorProducto) {
		String nombreProductoInput = JOptionPane.showInputDialog(null, "Nombre del producto a mostrar:");
		
		System.out.println("-");
		System.out.println("PRODUCTO:");
		System.out.println("Nombre: " + nombreProductoInput);
		System.out.println("Precio: " + contenedorProducto.get(nombreProductoInput));
		System.out.println("-");
	}
	
	//Metodo que recorre el hashtable y muestra el nombre y precio de cada producto por consola
	public static void mostrarTodos(Hashtable<String, Double> contenedorProducto) {
		Enumeration<String> keys = contenedorProducto.keys();
		Enumeration<Double> enumeration = contenedorProducto.elements();
		
		System.out.println("TODOS LOS PRODUCTOS:");
		while(enumeration.hasMoreElements()) {
			System.out.println("Nombre: " + keys.nextElement());
			System.out.println("Precio: " + enumeration.nextElement());
			System.out.println("----------");
		}
	}
}
