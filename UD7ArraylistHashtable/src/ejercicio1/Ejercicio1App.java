package ejercicio1;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.swing.JOptionPane;

public class Ejercicio1App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String cantidadAlumnosInput = JOptionPane.showInputDialog(null, "De cuantos alumnos quieres calcular sus medias?");
		int cantidadAlumnosInt = Integer.parseInt(cantidadAlumnosInput);
		
		String cantidadNotasInput = JOptionPane.showInputDialog(null, "Cuantas notas quieres introducir?");
		int cantidadNotasInt = Integer.parseInt(cantidadNotasInput);
		
		//Creamos un array para almacenar las notas
		double arrayNotas[] = new double[cantidadNotasInt];
		
		//Creamos un hashtable donde almacenaremos el nombre y la nota media de cada alumno
		Hashtable<String,Double> contenedorNotas = new Hashtable<String,Double>();
		
		//Pedimos al usuario el nombre del alumno tantas veces como cantidad de alumnos introducidos
		for (int contAlumn = 0; contAlumn < cantidadAlumnosInt; contAlumn++) {
			String nombreInput = JOptionPane.showInputDialog(null, "Introduce el nombre de un alumno:");
			
			//Pedimos al usuario las notas tantas veces como cantidad de notas introducidas
			for (int contNotas = 0; contNotas < cantidadNotasInt; contNotas++) {
				String notaInput = JOptionPane.showInputDialog(null, "Introduce la nota " + contNotas + "/" + cantidadNotasInt);
				arrayNotas[contNotas] = Double.parseDouble(notaInput);
			}
			
			//Llamamos al metodo de guardado de datos para que nos almacene la nota media (que tambien recibimos al llamar al metodo de calculo de esta) en el hashtable
			guardarDatos(contenedorNotas, nombreInput, calcularMedia(arrayNotas, cantidadNotasInt));
			
			//Volvemos a crear al array para "limpiar" las notas del alumno y introducir las del nuevo alumno en la siguiente vuelta del bucle
			arrayNotas = new double[cantidadNotasInt];
		}
		
		//Llamamos al metodo de mostrar el hashtable para que nos muestre el nombre del alumno y la nota media
		System.out.println("NOTAS MEDIAS:");
		mostrarContenedor(contenedorNotas);
	}
	
	//Metodo que calcula la media de las notas del array recibido
	public static double calcularMedia(double[] arrayNotas, int cantidadNotasInt) {
		double media = 0;
		
		for (int cont = 0; cont < arrayNotas.length; cont++) {
			media += arrayNotas[cont];
		}
		
		return media/cantidadNotasInt;
	}
	
	//Metodo que guarda el nombre de alumno y nota media en el array recibidos
	public static void guardarDatos(Hashtable<String, Double> contenedorNotas, String nombreInput, double media) {
		contenedorNotas.put(nombreInput, media);
	}
	
	//Metodo que muestra por consola el nombre y nota media del hashtable recibido
	public static void mostrarContenedor(Hashtable<String, Double> contenedorNotas) {
		Enumeration<String> keys = contenedorNotas.keys();
		Enumeration<Double> enumeration = contenedorNotas.elements();
		
		while(enumeration.hasMoreElements()) {
			System.out.println(keys.nextElement() + ": " + enumeration.nextElement());
		}
	}
}
