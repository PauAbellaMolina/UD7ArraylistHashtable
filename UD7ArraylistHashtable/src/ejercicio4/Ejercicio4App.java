package ejercicio4;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class Ejercicio4App {
	public static void main(String[] args) {
		//Creamos el hashtable y lo rellenamos con 10 productos de antemano
		Hashtable<String, Double> contenedorProductos = new Hashtable<String, Double>(){
		    {
		        put("iPhone X",1099.0);
		        put("Play Station 5",499.0);
		        put("EarPods",14.99);
		        put("Samsung Note 10",749.0);
		        put("Lenovo Thinkpad",999.99);
		        put("Logitech G403",34.99);
		        put("Ducky One TKL",139.0);
		        put("Audio Technica ATH-M40x",89.0);
		        put("Nvidia RTX 2080",799.99);
		        put("Windows 10 Key",109.0);
		    }
		};
		
		//Pedimos que el usuario nos introduzca una opcion del menu
		String opcionMenuInput;
		
		do {
			opcionMenuInput = JOptionPane.showInputDialog(null, "1) A�adir / 2) Mostrar Producto / 3) Mostrar Todos / 4) Seccion de compra / 5) Salir");
			
			switch (opcionMenuInput) {
				case "1":
					//Llamamos al metodo de anadir producto
					anadirProducto(contenedorProductos);
					break;
				case "2":
					//Llamamos al metodo de mostrar un producto concreto
					mostrarProducto(contenedorProductos);
					break;
				case "3":
					//Llamamos al metodo de mostrar todos los productos
					mostrarTodos(contenedorProductos);
					break;
				case "4":
					//Llamamos a una funcion que "ejecuta" todo el proceso de compra de un producto
					seccionCompra(contenedorProductos);
					break;
			}
		} while(!opcionMenuInput.contentEquals("5"));
	}
	
	
	//Metodo de compra de un producto. Esto es el ejercicio 2 basicamente.
	public static void seccionCompra(Hashtable<String, Double> contenedorProducto) {
		//Pedimos variables al usuario
		String cantidadProductosInput = JOptionPane.showInputDialog(null, "Cuantos productos vas a comprar?");
		int cantidadProductosInt = Integer.parseInt(cantidadProductosInput);
		
		double precioTotalProductos = 0;
		
		//Creamos el array para almacenar los datos del producto
		String[] arrayProducto = new String[4];
		
		//Creamos el ArrayList para almacenar cada array con los datos del producto
		ArrayList<String[]> lista = new ArrayList<>();
		
		//Bucle que pregunta al usuario el nombre y el IVA
		for (int contProductos = 0; contProductos < cantidadProductosInt; contProductos++) {
			String nombreProductoInput = "";
			//Comporvamos que el producto que quiere comprar esta en stock, es decir, existe en el hashtable
			do {
				nombreProductoInput = JOptionPane.showInputDialog(null, "Introduce el nombre del producto:");
			} while(contenedorProducto.get(nombreProductoInput) == null);
			arrayProducto[0] = nombreProductoInput;
			
			String ivaProductoInput = "";
			do {
				ivaProductoInput = JOptionPane.showInputDialog(null, "Introduce el IVA aplicado (21% o 4%):");
			} while(!ivaProductoInput.contentEquals("21") && !ivaProductoInput.contentEquals("4"));
			arrayProducto[1] = ivaProductoInput;
			
			//Cojemos el precio del producto del hashtable
			String precioProductoInput = Double.toString(contenedorProducto.get(nombreProductoInput));
			arrayProducto[2] = precioProductoInput;
			
			//Calculamos el precio con IVA
			double precioTotalIva = Double.parseDouble(precioProductoInput)+(Double.parseDouble(precioProductoInput)*Double.parseDouble(ivaProductoInput)/100);
			arrayProducto[3] = Double.toString(precioTotalIva);
			precioTotalProductos += precioTotalIva;
			
			//Guardamos dentro del ArrayList el array con los datos que nos acaba de introducir el usuario
			lista.add(arrayProducto);
			
			//"Limpiamos" el array para el siguiente paso del bucle
			arrayProducto = new String[4];
		}
		
		String cantidadPagadaInput = "";
		double cantidadPagadaInt = 0;
		//Controlamos que el dinero que nos da el cliente es mas que la cantidad a pagar
		do {
			cantidadPagadaInput = JOptionPane.showInputDialog(null, "Cantidad a pagar: " + precioTotalProductos + ". Dame el dinero:");
			cantidadPagadaInt = Double.parseDouble(cantidadPagadaInput);
		} while(cantidadPagadaInt < precioTotalProductos);
		
		//Calculamos el cambio a devolver
		double cambioDevolver = cantidadPagadaInt - precioTotalProductos;
		
		//Bucle que recorre cada posicion del ArrayList (es decir cada array de producto) y nos printea por consola los datos de cada producto
		System.out.println("-PRODUCTOS-");
		for (int i = 0; i < cantidadProductosInt; i++) {
			System.out.println("Nombre: " + lista.get(i)[0]);
			System.out.println("Tipo IVA: " + lista.get(i)[1]);
			System.out.println("Precio Bruto: " + lista.get(i)[2]);
			System.out.println("Precio con IVA: " + lista.get(i)[3]);
			System.out.println("");
			System.out.println("-----------------");
			System.out.println("");
		}
		
		System.out.println("Numero de articulos comprados: " + cantidadProductosInt);
		System.out.println("Cantidad Pagada: " + cantidadPagadaInt);
		System.out.println("Cambio a devolver: " + cambioDevolver);
		System.out.println("");
	}
	
	
	//Metodo que pregunta al usuario el nombre y precio de un producto nuevo y lo anade al hashtable
	public static void anadirProducto(Hashtable<String, Double> contenedorProducto) {
		String nombreProductoInput = JOptionPane.showInputDialog(null, "Nombre del producto:");
		String precioProductoInput = JOptionPane.showInputDialog(null, "Precio del producto:");
		double precioProductoInt = Double.parseDouble(precioProductoInput);
		
		contenedorProducto.put(nombreProductoInput, precioProductoInt);
	}
	
	//Metodo que pregunta al usuario el nombre del producto a mostrar y muestra su nombre y precio por consola
	public static void mostrarProducto(Hashtable<String, Double> contenedorProducto) {
		String nombreProductoInput = JOptionPane.showInputDialog(null, "Nombre del producto a mostrar:");
		
		System.out.println("-");
		System.out.println("PRODUCTO:");
		System.out.println("Nombre: " + nombreProductoInput);
		System.out.println("Precio: " + contenedorProducto.get(nombreProductoInput));
		System.out.println("-");
	}
	
	//Metodo que recorre el hashtable y muestra el nombre y precio de cada producto por consola
	public static void mostrarTodos(Hashtable<String, Double> contenedorProducto) {
		Enumeration<String> keys = contenedorProducto.keys();
		Enumeration<Double> enumeration = contenedorProducto.elements();
		
		System.out.println("TODOS LOS PRODUCTOS:");
		while(enumeration.hasMoreElements()) {
			System.out.println("Nombre: " + keys.nextElement());
			System.out.println("Precio: " + enumeration.nextElement());
			System.out.println("----------");
		}
	}
}
